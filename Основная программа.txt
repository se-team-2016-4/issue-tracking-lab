uses TaskModule;

var
  size, quote: Integer;
  TaskArray: TypeArray;
  dimension: Char;
begin
  Randomize;
  size := random(1000)+1;
  CreateArray(TaskArray, size);
  PrintArray(TaskArray, size);
  WriteLn();
  Write('Max: ');
  PrintCount(FindMax(TaskArray, size) );
  Write('Min: ');
  PrintCount(FindMin(TaskArray, size));
  Write('Middle: ');
  PrintCount(FindMiddle(TaskArray, size));
  Write('Enter quote: ');
  Readln(quote);
  Write('Enter dimension: ');
  ReadLn(dimension);
  QuoteComparison(TaskArray, size, quote, dimension);
  ReadLn;
end.